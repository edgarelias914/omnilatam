from apps.utils.models import OmniModel
from apps.users.models import User
from django.db import models

#  users, products, orders, payments, and shipments

class Product(OmniModel):
    name = models.CharField(max_length=255)
    reference = models.CharField(max_length=255)
    cost = models.FloatField()
    def __str__(self):
        return self.name

    class Meta:
        db_table = 'sales_product'
        managed = True
        verbose_name = 'Product'
        verbose_name_plural = 'Products'

class Order(OmniModel):
    client = models.ForeignKey(to=User, on_delete=models.PROTECT)
    shipment_state_complete = models.BooleanField(default=False)
    price = models.FloatField()
    def __str__(self):
        pass

    class Meta:
        db_table = 'sales_order'
        managed = True
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'

class OrderProduct(OmniModel):
    product = models.ForeignKey(to=Product, on_delete=models.PROTECT)
    order = models.ForeignKey(to=Order, on_delete=models.PROTECT, related_name='orders_products')
    price = models.FloatField()
    qty = models.FloatField()
    shipment_state = models.BooleanField(default=False)
    def __str__(self):
        return f"{self.user} - {self.product}"

    class Meta:
        db_table = 'sales_order_product'
        managed = True
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'
    
    def get_sub_total(self):
        return self.qty * self.price

class Shipment(OmniModel):
    address = models.CharField(max_length=255)
    client = models.ForeignKey(to=User, on_delete=models.PROTECT)
    def __str__(self):
        pass

    class Meta:
        db_table = 'sales_shipment'
        managed = True
        verbose_name = 'Shipment'
        verbose_name_plural = 'Shipments'

class ShipmentOrderProduct(OmniModel):
    shipment = models.ForeignKey(to=Shipment, on_delete=models.PROTECT, related_name='shipments_orders_products')
    order_product = models.ForeignKey(to=OrderProduct, on_delete=models.PROTECT)
    received = models.BooleanField(default=False)
    def __str__(self):
        pass

    class Meta:
        db_table = 'sales_shipment_order_producto'
        managed = True
        verbose_name = 'ShipmentOrderProduct'
        verbose_name_plural = 'ShipmentOrderProducts'

class Payment(OmniModel):
    client = models.ForeignKey(to=User, on_delete=models.PROTECT)
    paid_value = models.FloatField()
    def __str__(self):
        pass

    class Meta:
        db_table = 'sales_payment'
        managed = True
        verbose_name = 'Payment'
        verbose_name_plural = 'Payments'


class PaymentOrder(OmniModel):
    order = models.ForeignKey(to=Order, on_delete=models.PROTECT)
    payment = models.ForeignKey(to=Payment, on_delete=models.PROTECT, related_name='orders_payments')
    def __str__(self):
        pass

    class Meta:
        db_table = 'sales_order_payment'
        managed = True
        verbose_name = 'PaymentOrder'
        verbose_name_plural = 'PaymentOrders'