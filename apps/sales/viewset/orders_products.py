# Django REST Framework
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
# Serializers
from apps.sales.serialaizers import OrderModelSerializer

# Models
from apps.sales.models import Order, OrderProduct

class OrderProductViewSet(viewsets.ModelViewSet):
    
    serializer_class = OrderModelSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        """Restrict list to active products"""
        queryset = Order.objects.all()
        if self.action == 'list':
            return queryset.filter(active=True)
        return queryset
    
    def perform_destroy(self, instance):
        instance.active = False
        OrderProduct.objects.filter(order=instance).update(active=False)
        instance.save()