# Django REST Framework
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
# Serializers
from apps.sales.serialaizers import ProductModelSerializer

# Models
from apps.sales.models import Product

class ProductViewSet(viewsets.ModelViewSet):
    
    serializer_class = ProductModelSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        """Restrict list to active products"""
        queryset = Product.objects.all()
        if self.action == 'list':
            return queryset.filter(active=True)
        return queryset
    
    def perform_destroy(self, instance):
        instance.active = False
        instance.save()