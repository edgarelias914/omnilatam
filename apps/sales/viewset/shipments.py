# Django REST Framework
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
# Serializers
from apps.sales.serialaizers import ShipmentModelSerializer

# Models
from apps.sales.models import Shipment, ShipmentOrderProduct

class ShipmentViewSet(viewsets.ModelViewSet):
    
    serializer_class = ShipmentModelSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        """Restrict list to active products"""
        queryset = Shipment.objects.all()
        if self.action == 'list':
            return queryset.filter(active=True)
        return queryset
    
    def perform_destroy(self, instance):
        instance.active = False
        ShipmentOrderProduct.objects.filter(shipment=instance).update(active=False)
        instance.save()