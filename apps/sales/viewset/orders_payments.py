# Django REST Framework
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
# Serializers
from apps.sales.serialaizers import PaymentModelSerializer

# Models
from apps.sales.models import Payment, PaymentOrder

class PaymentOrderViewSet(viewsets.ModelViewSet):
    
    serializer_class = PaymentModelSerializer
    permission_classes = (IsAuthenticated, )

    def get_queryset(self):
        """Restrict list to active products"""
        queryset = Payment.objects.all()
        if self.action == 'list':
            return queryset.filter(active=True)
        return queryset
    
    def perform_destroy(self, instance):
        instance.active = False
        PaymentOrder.objects.filter(payment=instance).update(active=False)
        instance.save()