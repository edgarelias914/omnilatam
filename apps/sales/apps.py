from django.apps import AppConfig


class SalesAppConfig(AppConfig):
    name = 'apps.sales'
    verbose_name = 'Sales'
