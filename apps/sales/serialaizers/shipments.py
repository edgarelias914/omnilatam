# Django REST Framework
from rest_framework import serializers

# Models
from apps.sales.models import OrderProduct, Shipment, ShipmentOrderProduct

# Django
from django.conf import settings
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

class ShipmentOrderProductModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShipmentOrderProduct
        fields = [ 'shipment', 'order_product', 'received']
        read_only_fields = ['shipment', 'received']

class ShipmentModelSerializer(serializers.ModelSerializer):
    """
    Shipment model serializer
    """
    shipments_orders_products = ShipmentOrderProductModelSerializer(many=True)
    class Meta:
        model = Shipment
        fields = ('shipments_orders_products',)
    
    def create(self, validated_data):
        request = self.context['request']
        user =  request.user
        sop_data = validated_data.pop('shipments_orders_products')
        shipment = Shipment.objects.create(client=user, address=user.profile.address, **validated_data)
        for shipment_order_product in sop_data:
            ShipmentOrderProduct.objects.create(shipment=shipment, **shipment_order_product)
        self.send_email(user, 'email_send_shipment.html')
        return shipment
    
    def update(self, instance, validated_data):
        sop = self.context['request'].data
        received = False
        for shipment_order_product in sop.get('shipments_orders_products'):
            pk = shipment_order_product.pop('id')
            obj = ShipmentOrderProduct.objects.get(pk=pk) 
            received = shipment_order_product.get('received') is True and obj.received is False
            ShipmentOrderProduct.objects.filter(
                pk=pk).update(**shipment_order_product)
        if received:
            self.send_email(instance.client, 'email_recived_shipment.html')
        return instance
    
    
    def send_email(self, user, template):
        """
        Email shipment send confirm
        """
        subject = f'Hello @{user.username}!'
        from_email = f'OmniLatam - Test <{settings.DEFAULT_FROM_EMAIL}>'
        content = render_to_string(
            'emails/'+template,
            {'user': user}
        )
        msg = EmailMultiAlternatives(subject, content, from_email, [user.email])
        msg.attach_alternative(content, "text/html")
        msg.send()