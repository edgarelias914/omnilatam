# Django REST Framework
from rest_framework import serializers

# Models
from apps.sales.models import Payment, PaymentOrder

# Django
from django.db.models import Sum

class PaymentOrderModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = PaymentOrder
        fields = ['order', 'payment']
        read_only_fields = ['payment']

class PaymentModelSerializer(serializers.ModelSerializer):
    """
    Payment model serializer
    """
    orders_payments = PaymentOrderModelSerializer(many=True)
    class Meta:
        model = Payment
        fields = ('orders_payments',)
    
    def create(self, validated_data):
        request = self.context['request']
        user =  request.user
        pyo_data = validated_data.pop('orders_payments')
        payment = Payment.objects.create(client=user, paid_value=0, **validated_data)
        for payment_order in pyo_data:
            PaymentOrder.objects.create(payment=payment, **payment_order)
        
        self.payment_paid_update(payment)
        return payment
    
    def update(self, instance, validated_data):
        # pyo_data = validated_data.pop('orders_payments')
        opay = self.context['request'].data
        for payment_order in opay.get('orders_payments'):
            pk = payment_order.pop('id')
            PaymentOrder.objects.filter(
                pk=pk).update(**payment_order)
        
        self.payment_paid_update(instance)
        return instance
    
    def payment_paid_update(self, payment):
        total = PaymentOrder.objects.filter(payment=payment).aggregate(Sum('order__price'))
        payment.paid_value = total.get('order__price__sum')
        payment.save()