# Django REST Framework
from rest_framework import serializers

# Models

from apps.sales.models import Product

class ProductModelSerializer(serializers.ModelSerializer):
    """
    Product model serializer
    """
    class Meta:
        model = Product
        fields = ('name', 'reference', 'cost', )
    
    def create(self, validated_data):
        validated_data['name'] = validated_data['name'].upper()
        if Product.objects.filter(name=validated_data['name'], active=True).exists():
            raise serializers.ValidationError('Product name exist')
        return super().create(validated_data)