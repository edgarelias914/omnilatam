# Django REST Framework
from rest_framework import serializers

# Models
from apps.sales.models import Order, OrderProduct, Product

# Django
from django.db.models import Sum

class OrderProductModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderProduct
        fields = ['product', 'qty', 'id', 'order', 'price', 'shipment_state']
        read_only_fields = ['order','id', 'price', 'shipment_state']

class OrderModelSerializer(serializers.ModelSerializer):
    """
    Order model serializer
    """
    orders_products = OrderProductModelSerializer(many=True)
    class Meta:
        model = Order
        fields = ('orders_products',)
    
    def create(self, validated_data):
        request = self.context['request']
        user =  request.user
        op_data = validated_data.pop('orders_products')
        order = Order.objects.create(client=user, price=0, **validated_data)
        for order_product in op_data:
            product = order_product.get('product')
            OrderProduct.objects.create(order=order, price=product.cost*order_product.get('qty'), **order_product)
        
        self.order_price_update(order)
        return order
    
    def update(self, instance, validated_data):
        # op_data = validated_data.pop('orders_products')
        op = self.context['request'].data
        for order_product in op.get('orders_products'):
            pk = order_product.pop('id')
            product = Product.objects.get(pk=order_product.get('product'))
            order_product['price'] = product.cost * order_product.get('qty')
            OrderProduct.objects.filter(
                pk=pk).update(**order_product)
        
        self.order_price_update(instance)
        return instance
    
    def order_price_update(self, order):
        total = OrderProduct.objects.filter(order=order).aggregate(Sum('price'))
        order.price = total.get('price__sum', 0)
        order.save()