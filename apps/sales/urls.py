# Django REST Framework
from rest_framework.routers import DefaultRouter

# Views
from .viewset import (
    products as products_view,
    orders_products as op_view,
    shipments as shipment_view,
    orders_payments as payment_view
)

# Django
from django.urls import path, include

router = DefaultRouter()
router.register(r'products', products_view.ProductViewSet, basename='products')
router.register(r'orders_products', op_view.OrderProductViewSet, basename='orders_products')
router.register(r'shipments_orders_products', shipment_view.ShipmentViewSet, basename='shipments_orders_products')
router.register(r'orders_payments', payment_view.PaymentOrderViewSet, basename='orders_payments')

urlpatterns = [
    path('', include(router.urls))
]