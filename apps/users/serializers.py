# Django
from django.contrib.auth import password_validation, authenticate

# Django REST Framework
from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework.authtoken.models import Token

# Models
from apps.users.models import User, Profile

class ProfileModelSerializer(serializers.ModelSerializer):
    """
    Profile model serializer
    """
    class Meta:
        model = Profile
        fields = (
            'identification',
            'address',
            'biography',
        )

class UserModelSerializer(serializers.ModelSerializer):
    """
    Create user serializer
    """
    profile = ProfileModelSerializer(read_only=True)

    class Meta:
        model = User
        fields = ( 'username', 'first_name', 'last_name', 'email', 'profile' )

class UserLoginSerializer(serializers.Serializer):
    """
    User Login Serializer
    Handle the login request data
    """
    email = serializers.EmailField()
    password = serializers.CharField(min_length=8)
    def validate(self, data):
        """
        Check credentials
        """
        user = authenticate(username=data['email'], password=data['password'])
        if not user:
            raise serializers.ValidationError('Invalid credentials')
        self.context['user'] = user
        return data
    
    def create(self, validated_data):
        """
        Generate or retrieve new token
        """
        user = self.context['user']
        token, created = Token.objects.get_or_create(user=user)
        return user, token.key

class CreateUserSerializer(serializers.Serializer):
    """
    Create user serializer
    Handle create data validation and user/profile creation.
    """
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(
        min_length=4,
        max_length=20,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )

    # Password
    password = serializers.CharField(min_length=8, max_length=64)
    password_confirmation = serializers.CharField(min_length=8, max_length=64)

    # Name
    first_name = serializers.CharField(min_length=2, max_length=30)
    last_name = serializers.CharField(min_length=2, max_length=30)

    def validate(self, data):
        """Verify passwords match."""
        passwd = data['password']
        passwd_conf = data['password_confirmation']
        if passwd != passwd_conf:
            raise serializers.ValidationError("Passwords don't match.")
        password_validation.validate_password(passwd)
        return data

    def create(self, data):
        """Handle user and profile creation."""
        data.pop('password_confirmation')
        user = User.objects.create_user(**data, is_client=True)
        Profile.objects.create(user=user)
        return user

