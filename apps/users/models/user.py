from django.core import validators
from django.db import models
# Create your models here.

from django.contrib.auth.models import AbstractUser
from apps.utils.models import OmniModel
from django.core.validators import RegexValidator

class User(OmniModel, AbstractUser):
    email = models.EmailField(
        verbose_name = 'Email Address',
        unique=True,
        error_messages={
            'unique': 'A user with that email already exits'
        }
    )
    is_client = models.BooleanField(
        verbose_name='Cliente',
        default=False,
        help_text=(
            'Help easily distinguish users and perform queries. '
            'Clients are the main type of user'
        )
    )
    
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']
    
    class Meta:
        db_table = 'auth_user'
        managed = True
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'
    
    def __str__(self):
        return self.username
    
    def get_short_name(self) -> str:
        return super().get_short_name()
