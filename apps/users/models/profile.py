from django.db import models
from apps.utils.models import OmniModel
from apps.users.models.user import User

class Profile(OmniModel):
    user = models.OneToOneField(to=User, on_delete=models.PROTECT)
    identification = models.CharField(max_length=100, blank=True)
    address = models.CharField( max_length=225, blank=True )
    biography = models.TextField( max_length=500, blank=True )
    
    class Meta:
        db_table = 'usr_profile'
        managed = True
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfiles'
    
    def __str__(self):
        return str(self.user)
    