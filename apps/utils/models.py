"""Django models utilities"""

#Django

from django.db import models

class OmniModel(models.Model):

    created = models.DateTimeField(
        verbose_name='created at',
        auto_now_add=True,
        help_text='Date time on which the object was created.'
    )
    updated = models.DateTimeField(
        verbose_name='updated at',
        auto_now_add=True,
        help_text='Date time on which the object was last updated.'
    )
    active = models.BooleanField(
        verbose_name='active',
        default=True
    )
    class Meta:
        abstract = True
        get_latest_by = ['created']
        ordering = ['-created', '-updated']
    