#!/bin/bash
# Make migrations and migrate
if [[ -z $AUTH_ONLY ]]; then
  python manage.py makemigrations --no-input
  mkmg_status=$?
  if [ $mkmg_status -ne 0 ]
  then
    echo $mkmg_status
    exit $mkmg_status
  fi

  python manage.py migrate --no-input
  # Collect static files
  python manage.py collectstatic --no-input
fi
# uvicorn server
gunicorn omnilatam.asgi:application -k uvicorn.workers.UvicornWorker --workers 2 --threads 4 --bind 0.0.0.0:8000 --access-logfile '-' --error-logfile '-' --forwarded-allow-ips '*' --access-logformat '%({x-forwarded-for}i)s %(l)s %(u)s %(t)s "%(r)s" %(s)s %(b)s "%(f)s" "%(a)s"'
