# Omni Latam
###### Omni is a Financial Technology Company that provides predictions and recommendations.
Context: A company requires a relational database to manage users, products, orders,
payments, and shipments. Please refer only to the minimum needed attributes for an ecommerce flow

## Considerations

- A user can purchase products as part of an order.
- A payment can apply to one or more orders and an order can be paid by one or more payments.
- An order can delivered by one or more shipments.
- A notification to the purchasers (users) should be sent when a shipment is sent/received.

## Requirements:
- The Application should include a RESTful API.
- The Application should include SIGN-IN/OUT.
- The Application should include all CRUD operations for all entities.
- The APIs should be paginated.
- The Application should include an example of at least one asynchronous process. (optional).
- Provide a SQL report showing the paid and/or sent orders info.
- The code should contain tests.

## Installation

Dillinger requires [Docker](https://docs.docker.com/get-docker/) and [Docker-compose](https://docs.docker.com/compose/) to run.

Install the dependencies and devDependencies and start the server.


```
git clone https://edgarelias914@bitbucket.org/epena1490/omnilatam.git
cd omnilatam
env UID=$(id -u) docker-compose up -d --build
docker-compose exec django python manage.py createsuperuser
```

Verify the deployment by navigating to your server address in
your preferred browser.

```
127.0.0.1:8000
```
