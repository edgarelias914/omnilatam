-- Report showing the paid order
SELECT ord."id", ord.created, ord.shipment_state_complete, ord.price, us.email, us.first_name, us.last_name 
FROM sales_order_payment AS sop
INNER JOIN sales_order AS ord
ON sop.order_id = ord."id"
INNER JOIN auth_user AS us
ON us."id" = ord.client_id
WHERE sop.active
;
-- Report showing the sent order
SELECT ord."id", ord.created, ord.shipment_state_complete, ord.price,
op.price, pd."name", sp.address, us.email, us.first_name, us.last_name 
FROM sales_shipment_order_producto AS sop
INNER JOIN sales_order_product AS op
ON sop.order_product_id = op."id"
INNER JOIN sales_product AS pd
ON pd."id" = op.product_id
INNER JOIN sales_order AS ord
ON ord."id" = op.order_id
INNER JOIN sales_shipment AS sp
ON sp."id" = sop.shipment_id
INNER JOIN auth_user AS us
ON us."id" = ord.client_id
WHERE sop.active

