pytz==2021.3

#PostgreSQL
psycopg2==2.9.1

#Django
Django==3.2.8

#Django enviroments
django-environ==0.7.0

#Django rest framework
djangorestframework==3.12.4

#Security
argon2-cffi==21.1.0
