# For more information, please refer to https://aka.ms/vscode-docker-python
FROM bitnami/python:3.9-prod-debian-10

ARG UID

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

# Install pip requirements
RUN install_packages build-essential libpq-dev libldap2-dev libsasl2-dev

RUN install_packages gcc python3-dev musl-dev
RUN install_packages libffi-dev python-cffi
RUN install_packages postgresql-client


COPY requirements.txt .
RUN pip install --no-cache-dir --upgrade pip
RUN pip --no-cache-dir install gunicorn uvicorn[standard] && pip --no-cache-dir install -r requirements.txt

WORKDIR /app

# Creates a non-root user with an explicit UID and adds permission to access the /app folder
RUN useradd -u ${UID} appuser && chown -R appuser /app
USER appuser

EXPOSE 8000

COPY entrypoint.sh .
CMD ["./entrypoint.sh"]
